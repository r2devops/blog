---
template: blog_list.html
title: All the articles
description: > 
    Welcome on R2Devops blog. Here we talk about CI/CD, DevOps... all subjects related to R2Devops! 
---

## Discover our latest articles


---


<!-- Plausible -->
<script defer data-domain="blog.r2devops.io" src="https://plausible.io/js/plausible.js"></script>
<!-- End Plausible -->