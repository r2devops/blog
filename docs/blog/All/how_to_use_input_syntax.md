---
template: blog_post.html
picture: ../img/how_to_use_input_syntax.png
title: Supercharge CI templates with the new input syntax
description: >
    Learn how to create GitLab CI templates with input parameters in R2Devops, leveraging the new syntax introduced in GitLab 15.11
date: 2023-04-26
---
<p hidden>#more</p>

## Introduction

GitLab recently released version 15.11, [introducing a new
format](https://about.gitlab.com/releases/2023/04/22/gitlab-15-11-released/#define-inputs-for-included-cicd-configuration)
to define input parameters for CI/CD configuration templates. This improves the flexibility and reusability of pipeline templates.

As the GitLab CI Marketplace, we are excited to announce that R2Devops already
supports this new feature.

In this blog post, we'll walk you
through how to create a template with input parameters in R2Devops, using the
new syntax, and demonstrate its usage.

## Getting started with input parameters in GitLab CI

### 1. How it works

The new input parameters syntax allows defining optional or mandatory
inputs for CI/CD templates. Here's a quick example of the new format:

```yaml title="Template definition"
# 1. Input parameters definition

spec:
  inputs:
    project_root:
    node_version:
      default: "20"

---

# 2. Template definition
# You can use input parameters with following syntax: $[[ inputs.variable_name ]]

npm_test:
  image: node:$[[ inputs.node_version ]]
  script:
    - cd $[[ inputs.project_root ]]
    - npm install
    - npm run test
```

In this example, the template defines 2 input parameters:

- `project_root` which is mandatory because no default value is defined
- `node_version` which is not mandatory thanks to the default value `20`

To use this template, you have to use the `with` keyword when including the
template:

```yaml title="Template usage in .gitlab-ci.yml"
include:
  - remote: 'https://api.r2devops.io/job/r/<link-to-template>@latest.yaml'
    with:
      project_root: "./my-react-project/"
      node_version: "16"
```

### 2. Create your template

Now that you're familiar with the new syntax, let's create a GitLab CI template
with input parameters in R2Devops. This template will be a complete pipeline
running test, build and deploy for a React project.

An example of template is available
[here](https://r2devops.io/marketplace/gitlab/r2devops/incubator/node) with
sources in [this repository](https://gitlab.com/r2devops/incubator)

1. **🦊 Create a GitLab repository**

    This repository will contain your template. You can use an already existing
    repo if you want

2. **🛠️ Create your template**

    At the root of your repository, create a file named `node-with-input.yml` with
    the following content:

    ```yaml title="node-with-input.yml"
    spec:
      inputs:
        project_root:
          default: "."
        node_version:
          default: "20"
        output_directory:
          default: "build"

    ---

    stages:
      - build
      - tests
      - deploy

    npm_test:
      stage: tests
      image:
        name: node:$[[ inputs.node_version ]]-buster
        entrypoint: [""]
      script:
        - cd $[[ inputs.project_root ]]
        - npm install
        - npm run test

    npm_build:
      stage: build
      image:
        name: node:$[[ inputs.node_version ]]-buster
        entrypoint: [""]
      script:
        - cd $[[ inputs.project_root ]]
        - npm install
        - npm run build
      artifacts:
        expose_as: "build"
        paths:
          - "$[[ inputs.project_root ]]/$[[ inputs.output_directory ]]"

    pages:
      stage: deploy
      script:
        - mv $[[ inputs.output_directory ]]  public
      artifacts:
        paths:
          - public
    ```

3. **⚙️ Create a R2 file**

    At the root of your repository, create a file named `node-with-input.r2.yml` with
    the following content:

    ```yaml title="node-with-input.r2.yml"
    files:
      template: ./node-with-input.yml
    data:
      description: "Build, test and deploy your frontend JavaScript projects"
      icon: 🧰
      public: true
      labels:
        - Node
        - Test
        - Build
        - Deploy
    ```

4. Commit & push both files in the default branch
5. **🧑‍💻 Login on [R2Devops](https://r2devops.io)**
6. **🔁 In "Import templates" page:**
    - Select root group of your project in `Organization` list
    - Click on `Import` for your project in the list
7. **🎉 Congrats ! Your template is now available in the marketplace**

### 3. Use your template

Now, let's use this template in one of your project !

*An example of project using template with input is available
[here](https://gitlab.com/r2devops/demos/react-using-template-with-input)*

1. Find your template by [searching its
   name](https://r2devops.io/marketplace?search=node-with-input) in the
   marketplace. Be sure that the `From` mention your repository.
2. Copy its link
3. Use it in the `.gitlab-ci.yml` of any GitLab project like this:

    ```yaml title=".gitlab-ci.yml"
    include:
      - remote: 'PASTE HERE YOUR TEMPLATE LINK'
        with:
          project_root: "."
          node_version: "16"
    ```

4. Commit and push your update on `.gitlab-ci.yml` file
5. Go to the CI/CD page of the project to see your pipeline ! 🥳

### 4. Example

- Example of template using the input syntax is available [here](https://r2devops.io/marketplace/gitlab/r2devops/incubator/node) ([sources](https://gitlab.com/r2devops/incubator/-/blob/main/templates/node/node.yml))
- Example of repository using this template [here](https://gitlab.com/r2devops/demos/react-using-template-with-input) ([`.gitlab-ci.yml`](https://gitlab.com/r2devops/demos/react-using-template-with-input/-/blob/main/.gitlab-ci.yml))

## Conclusion

GitLab's new input parameters syntax in CI/CD configuration templates makes it
easier than ever to create reusable pipeline configurations.

We are proud to support this new feature, enabling you to take full
advantage of our ever-growing marketplace of GitLab CI templates on R2Devops.

By following the steps in this tutorial, you have created and used templates
with input parameters to streamline your CI/CD pipelines and optimize your
development workflow.

Enjoy 🥳🥳
