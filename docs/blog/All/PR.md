---
template: blog_post.html
picture: ../img/r2devops-cover.png
title: R2Devops joins GitLab Inc.’s Alliance Partner Program
description: > 
    R2Devops partners with GitLab to support open source CI/CD jobs.
date: 2022-07-17
---
<p hidden>#more</p>


# R2Devops joins GitLab Inc.’s Alliance Partner Program

As a collaborative hub for open-source CI/CD jobs, we had joined [GitLab](https://about.gitlab.com/) Inc.’s Alliance Partner Program. GitLab is The One DevOps Platform for software innovation. Together, we ease the CI/CD pipeline onboarding and management process for users.

“This partnership reinforces our shared values of collaboration, efficiency, and transparency. It enables all developers on GitLab to access open source and collaborative jobs and enhance CI/CD practices”. Aurélien Coget, CEO & Cofounder, R2Devops


Creating your pipeline becomes easier with R2Devops. Developers can take advantage of community resources as part of the CI/CD journey and focus more on what they love, coding! Our comprehensive documentation explains how to use a job, integrate it into your GitLab pipeline via an include link and customize it. 


Collaboration and transparency are the cornerstone of the platform – anyone can contribute from their GitLab repository and share open source CI/CD jobs with the entire developer community.  


“As an open source company, GitLab highly values the community and is pleased to partner with organizations that align with our transparency value and promote open source collaboration,” said Mike LeBeau, Sr. Alliance Manager at GitLab.


More than a hub, developers can ask R2Devops to build a CI/CD pipeline for their project in a few clicks. The R2Devops algorithm analyzes the project DNA and picks the best certified open source jobs adapted for the project in the hub. 



## About R2Devops
Relying on our strong values of sharing, transparency and efficiency, R2Devops’ community allows us to think user-centric and focus on what really matters: improve and simplify developer's daily life. Launched in November 2020, the first version of the platform counts more than 200k jobs used everywhere in the world.
For more information on what we're doing, visit [r2devops.io](https://r2devops.io) or join the [discord community](https://discord.r2devops.io/?utm_medium=gitlab&utm_source=pressrelease&utm_campaign=partner).
