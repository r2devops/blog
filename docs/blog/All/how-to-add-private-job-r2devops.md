---
template: blog_post.html
picture: ../img/r2devops-cover.png
title: How to add a private job in R2Devops?
description: > 
   Discover the two ways to add a private job in R2Devops and create your own registry of CI/CD jobs.
date: 2022-12-08
---
<p hidden>#more</p>

# Add a private job in R2Devops to build your GitLab CI/CD pipeline

R2Devops is a [library of open source and collaborative resources](https://r2devops.io/_/hub) you can use to **build your CI/CD pipeline in GitLab**. You can add any resources on your own, share them with the community and **support other developers in their DevOps journey**. 

Our latest improvement: you can make the resources you added in R2Devops private!

First thing first: What does it mean to have a [private job](https://docs.r2devops.io/faq/#private-job)?

!!! info "A private job is a resource only you and your GitLab organization can access and use."

      It’s link to the GitLab organization of the project where the code is located. If you are the only one working on this organization or if the project is located on your own namespace, only you will see it. **If other people are working with you, the resources will appear as a private job in their R2Devops account too.**
      
## How to add a private job in R2Devops?

Well, it’s easy. First, you need to create an account in R2Devops, and log in.

Once you are in your personal dashboard, go to the section **Resources - My jobs**.

!!! info

     If you never have linked a job in R2Devops before, you’ll see a panel in the screen. You just have to click on it to begin the process. If you already have a job in R2Devops, a plus sign appears on the top-right side of your screen.

From there, you just have to follow the guide!

1. [Add your personal token](https://docs.r2devops.io/faq/#access-token)
2. Fill your job data

!!! info
     
     On the panel number 2, where you will fill your job’s information, you can select if you want your job to be private or public. 

![Choose the visibility of your job in the job's information panel](../../img/choose_private_public_job.png)

3. Import your job and complete its information to fill the documentation!

Here you are, your private job was added in R2Devops.

!!! info "Where can I find my private job?"

     You can see and modify your private job from your personal dashboard, in the section **Resources - My jobs**, or in the **[hub](https://r2devops.io/_/hub)**, selecting Private jobs in the filters’ section.


## How to make a job private already in the hub?


In order to make a job already in the hub private, you need to go to the modify page.

!!! info

     This page is accessible from the job’s documentation or from your personal dashboard.

Once you are in the job’s modification page, scroll down. At the bottom of the page, there is a toggle to change your job from public to private (and reverse).

![Choose the visibility of your job in the job's documentation panel](../../img/private_job_documentation.gif)

!!! warning

     Be careful! Moving a job from public to private will broke the pipeline of people from the community using it!

You know where to start, so go ahead, and create your own registry of CI/CD resource you can share with your team!