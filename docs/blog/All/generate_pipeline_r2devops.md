---
template: blog_post.html
picture: ../img/r2devops-cover.png
title: Generate automatically your CI/CD pipeline using R2Devops
description: > 
    Learn how to create your CI/CD pipeline automaltically using R2Devops. It will be done in a minute! 
date: 2022-05-17
---
<p hidden>#more</p>

**⚠️The pipeline generator feature is currently disabled on R2Devops, join [our Discord](https://discord.r2devops.io?utm_medium=website&utm_source=r2devops_blog&utm_campaign=blog) to stay up-to-date⚠️**  

# Generate automatically your CI/CD pipeline using R2Devops

CI/CD has become a core component of development’s projects. It’s vital for any company that produces software and needs to scale. But building and maintaining your pipelines adds up to 20% of time to your development team, without adding value to your product.  To solve this issue, we have created Pipeline Generator! This feature automates the creation and maintenance of your pipeline! 👇

## How to use Pipeline Generator

Let’s see how you can automatically generate a CI/CD pipeline with R2Devops!

### Link your GitLab or GitHub account in R2Devops

#### Create your account in R2Devops

First things first: you need to create an account on R2Devops. You have 3 options:

* Continue with GitLab
* Continue with GitHub
* Register with your email address

#### Link your projects

Once it’s done, you’ll arrive to `My projects` page. To use Pipeline Generator feature and automatically generate a CI/CD pipeline, we need to access your projects. 

!!! info 

   Your projects will already be listed in your projects’ dashboard if you created your account by continuing with GitLab or GitHub. You can easily do it in your profile settings if you haven’t.

#### Add your platform’s personal token access

In order to automatically push the pipeline generated, we will need your [platform personal token access](https://docs.r2devops.io/faq-profile-token/).

!!! info 
   If you haven’t filled it already,  you will be asked to do it before you can access to the pipeline generator pop-up. You can easily manage it from your account page in your settings.

#### Generate your CI/CD pipeline

Choose the project you want, and click on “Generate a pipeline” at the end of the line!

A pop-up will open, asking you for the project’s branch we should analyze to build your pipeline (by default it’s main), and the destination branch, meaning the branch you want us to push your pipeline (by default r2devops-autopipeline).

!!! info

   You can choose to push it or not. At the end of the process, the code of your pipeline will be displayed either ways.

Click on Generate, et voilà, your pipeline is ready!

![Generate automatically your pipeline using R2Devops Pipeline Generator feature](../../img/Autopipeline_scheme.gif)

### Safe and efficient CI/CD jobs picked in R2Devops library

Our Pipeline Generator feature will automatically analyze the code of your source branch. Then, it will pick in R2Devops’ library official jobs that fit its need!

Official jobs respect [standards of quality and security](https://docs.r2devops.io/faq-use-a-job/), to ensure their efficiency. They are kept up-to-date by our internal teams and the community. Hundreds of developers are working on R2Devops!


![Generate automatically your pipeline using R2Devops Pipeline Generator feature](../../img/Security_simplified.png)

## The benefits of Pipeline Generator

As you saw, using Pipeline Generator is quite easy. In a few seconds, you have a functioning pipeline that fits your project requirements! But why is it helpful? What to do you gain using Pipeline Generator?

### Easy maintenance of your pipeline

Additionally to create your pipelines in a few minutes (instead of hours), it’s now really easy to maintain them! The only thing you have to do is to generate a new pipeline, and the jobs will be automatically to the last tagged version available.

For now, you have to do it on your own. But we plan to add notifications on your personal space to let you know when your pipeline will need an update. You won’t have to track any more all the changes made on the jobs you use, and check if they fit in your project. R2Devops' Pipeline Generator will do it for you!
### Save time and focus on what you love: coding!

The main benefit is the time your team gain. 

!!! info

     With R2Devops, every developer of your team can save up to 1 hour per day!

It’s up to you to choose how this time can be spent! Your developers can:

* focus on coding new functionalities for your product, 
* do research and add innovation,
* contribute to an open source project!

The possibilities are endless. And it all begins with the [creation of an account](https://r2devops.io/u/signin) in R2Devops!