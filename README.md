# R2Devops Blog - Created with [Material MkDocs](https://squidfunk.github.io/mkdocs-material/).

## Based on the creation of [4kelly](https://material-mkdocs-blog.4kelly.com/)

[MkDocs](https://www.mkdocs.org) is a static site generator. 
[Material MkDocs](https://squidfunk.github.io/mkdocs-material/) is a theme for MkDocs.
MkDocs has no built-in support for blogging, but it is extensible enough to easily add your own.
This repo is the experimental precursor to a MkDocs [plugin](https://www.mkdocs.org/dev-guide/plugins/), 
if there is demand.

🚀 Annotate your posts with a beautiful signature!

![Example Signature](docs/img/signature.png)

🚀 Turn any page into a list of blog posts. Checkout the [blog](https://material-mkdocs-blog.4kelly.com/blog/)! 

🚀 Standardize the look of each blog post with configurable metadata. Checkout a post [blog](https://material-mkdocs-blog.4kelly.com/blog/2021/cooking_post/)! 

--- 

For updates or feature requests, follow `@4kelly_` [Twitter](https://twitter.com/4kelly_).


## Setup & contribution

### What kind of article gets published into R2Devops?

We will publish any tech artcile related to CI/CD, open source and community improvements. You article can be directly related to R2Devops, or on the contrary talk about a generic subject. 

Follow the guide to fork the blog repository and add your article 👇🏻

### Starting From Scratch

1. Clone this repository locally.

    ```shell
    git clone 
    cd blog
    ```

2. Setup Dependencies

    ```shell
    # recommended to use a venv.
    pip install -r requirements.txt
    ```

3. Add a new blog post anywhere under the `/blog` directory and append the [required metadata](#configuring-a-blog-post).

    ```shell
    # A helper command will do this for you.
    cp first_post.txt docs/blog/2021/first_post.md
    ```

4. Add this new post to the `mkdocs.yml` navigation configuration.

If you can't find a category corresponding to your article's topic, fell free to create a new one.  

5. Start MkDocs and look at your new page in the browser!

    ```shell
    mkdocs serve
    ```
    Browser connected: [http://127.0.0.1:8000/](http://127.0.0.1:8000/)


### Configuring A Blog List

- After following the [quickstart](#setup),
add this template metadata to any page that you want to display a list of blog posts.

```markdown
---
template: blog_list.html
---

Add any markdown content here, the blog list will be displayed below it.
```

### Configuring A Blog Post

- After following the [quickstart](#setup), 
 add this template metadata to the page that you want to become a blog post.
- Any content in your blog post above the special `<p hidden>#more</p>` tag will be shown in the preview.
    - You can manually place this tag anywhere within your blog post. 

```markdown
---
template: blog_post.html
title: Cool post
description: Informative description
date: 1984-01-01
---

This content will be present in the `blog_list.html` template preview.

<p hidden>#more</p>

More content
```

`title`

:   Will be rendered in the `HTML` `<title>` tag, 
    and sets an `<h2>` tag in your blog post and blog list.

`description`

:   Will be rendered in the `HTML` `<meta description>` tag, 
    and sets a description under the title in your blog post and blog list.

`date`
:   Expected format: `YYYY-MM-DD`, rendered in the [signature](#blog-signature)

